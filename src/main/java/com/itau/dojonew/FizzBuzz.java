package com.itau.dojonew;

public class FizzBuzz {
	public String validaNumero (int numero) {

		if(restoPor3(numero) == 0 && restoPor5(numero) == 0) {
			return "FizzBuzz";
		} 

		if(restoPor3(numero) == 0){
			return "Fizz";
		}

		if(restoPor5(numero) == 0)
		{
			return "Buzz";
		}

		return Integer.toString(numero);

	}

	public int restoPor5(int numero) {
		return (numero % 5);
	}

	public int restoPor3(int numero) {
		return (numero % 3);
	}
	
	public String validaSequencial(int numero) {
		String fizzBuzz = "";
		for(int i=1; i <= numero; i++) {
			if (restoPor3(i) == 0 && restoPor5(i) == 0) {
				fizzBuzz += "FizzBuzz ";
			}else if (restoPor3(i) == 0) {
				fizzBuzz += "Fizz ";
			}else if (restoPor5(i) == 0){
				fizzBuzz += "Buzz ";
			}else {
				fizzBuzz += i+ " ";
			}
		}
		return fizzBuzz.substring (0, fizzBuzz.length() - 1);
	}
}
