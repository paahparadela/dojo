package com.itau.dojonew;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class FizzBuzzTest extends TestCase {
	
	FizzBuzz fizzBuzz = new FizzBuzz();
	String retorno;
	
	public FizzBuzzTest( String testName )
	{
		super( testName );
	}

	public static Test suite()
	{
		return new TestSuite( FizzBuzzTest.class );
	}
	
/*	@Test
	void testeBuzzValida5() {
		retorno = fizzBuzz.validaNumero(5);
		assertEquals("Buzz", retorno);	
	}
	@Test
	void testBuzzValida3() {
		retorno = fizzBuzz.validaNumero(3);
		assertEquals("Fizz", retorno);	
	}
	
	@Test
	void testBuzzValida3e5() {
		retorno = fizzBuzz.validaNumero(15);
		assertEquals("FizzBuzz", retorno);	
	}

	@Test
	void testBuzzValidaDiferente() {
		retorno = fizzBuzz.validaNumero(2);
		assertEquals("2", retorno);	
	}

	@Test
	void testBuzzValida6() {
		retorno = fizzBuzz.validaNumero(6);
		assertEquals("Fizz", retorno);	
	}

	@Test
	void testBuzzValida10() {
		retorno = fizzBuzz.validaNumero(10);
		assertEquals("Buzz", retorno);	
	}
		

	@Test
	void testBuzzValida30() {
		retorno = fizzBuzz.validaNumero(30);
		assertEquals("FizzBuzz", retorno);	
	}
	
	@Test
	void testBuzzValidaDiferente4() {
		retorno = fizzBuzz.validaNumero(4);
		assertEquals("4", retorno);	
	}*/
	
	public void testBuzzValidaSequencial3() {
		retorno = fizzBuzz.validaSequencial(3);
		System.out.println(retorno);
		assertEquals("1 2 Fizz", retorno);
	}
	
	public void testBuzzValidaSequencial5() {
		retorno = fizzBuzz.validaSequencial(5);
		System.out.println(retorno);
		assertEquals("1 2 Fizz 4 Buzz", retorno);
	}
	
	public void testBuzzValidaSequencial15() {
		retorno = fizzBuzz.validaSequencial(15);
		System.out.println(retorno);
		assertEquals("1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz", retorno);
	}
}
